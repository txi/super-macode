const getTopKeywords = require("../KeyWord/topkKeywords")

describe("Question 2 Test Cases", () => {

  test("should deal with normal cases", () => {
    const k = 2
    const keywords = ["anacell", "cetracular", "betacellular"]
    const reviews = [
      "Anacell provides the best services in the city",
      "betacellular has awesome services",
      "Best services provided by anacell, everyone should use anacell",
    ]

    expect(getTopKeywords(k, keywords, reviews)).toBe(["anacell", "betacellular"])
  })

  test("should deal with long cases", () => {
    const k = 2
    const keywords = ["anacell", "betacellular", "cetracular", "deltacellular", "eurocell"]
    const reviews = [
      "I love anacell Best services; Best services provided by anacell",
      "betacellular has great services",
      "deltacellular provides much better services than betacellular",
      "cetracular is worse than anacell",
      "Betacellular is better than deltacellular.",
    ]

    expect(getTopKeywords(k, keywords, reviews)).toBe(["betacellular", "anacell"])
  })
})