const arrayTransform = require("../ArrayTransform/ArrayTransform")

const example1 = [
  {
    "id": "w1",
    "menu_name": "首页",
    "menu_url": "home",
    "parent_id": null
  },
  {
    "id": "w1-1",
    "menu_name": "首页-工作台",
    "menu_url": "home.workbench",
    "parent_id": "w1"
  },
  {
    "id": "w1-2",
    "menu_name": "首页-工作日历",
    "menu_url": "home.calendar",
    "parent_id": "w1"
  },
  {
    "id": "w1-2/1",
    "menu_name": "首页-工作日历-面访",
    "menu_url": "home.calendar.interview",
    "parent_id": "w1-2"
  }
]

const example2 = [
  {
    "id": "w1",
    "menu_name": "首页",
    "menu_url": "home",
    "parent_id": null
  },
  {
    "id": "w1-1",
    "menu_name": "首页-工作台",
    "menu_url": "home.workbench",
    "parent_id": "w1"
  },
  {
    "id": "w1-2",
    "menu_name": "首页-工作日历",
    "menu_url": "home.calendar",
    "parent_id": "w1"
  },
]

describe("Question 1 Test Cases", () => {

  test("should deal with empty object", () => {
    expect(arrayTransform([])).toBe({})
  })

  test("should deal with normal case", () => {
    expect(arrayTransform(example1)).toStrictEqual({
      "id": "w1",
      "menu_name": "首页",
      "menu_url": "home",
      "parent_id": null,
      "children": [{
          "id": "w1-1",
          "menu_name": "首页-工作台",
          "menu_url": "home.workbench",
          "parent_id": "w1",
          "children": []
        },
        {
          "id": "w1-2",
          "menu_name": "首页-工作日历",
          "menu_url": "home.calendar",
          "parent_id": "w1",
          "children": [{
            "id": "w1-2/1",
            "menu_name": "首页-工作日历-面访",
            "menu_url": "home.calendar.interview",
            "parent_id": "w1-2"
          }]
        }
      ]
    })
  })

  test("should deal with no children case", () => {
    expect(arrayTransform(example2)).toStrictEqual({
      "id": "w1",
      "menu_name": "首页",
      "menu_url": "home",
      "parent_id": null,
      "children": [{
          "id": "w1-1",
          "menu_name": "首页-工作台",
          "menu_url": "home.workbench",
          "parent_id": "w1",
          "children": []
        },
        {
          "id": "w1-2",
          "menu_name": "首页-工作日历",
          "menu_url": "home.calendar",
          "parent_id": "w1",
          "children": []
        }
      ]
    })
  })
})
