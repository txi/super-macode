# 超级码力模拟题之前端


## Q1 将数组转化为树形结构
## 描述
初始时，数组中的每个元素具有 4 个属性，其中有 id 和 parent_id，现在我们需要根据这两个 id 之间的关系，添加一个 children 属性，使之成为一棵树的结构。
### 样例
> 输入
```json
[
  {
    "id": "w1",
    "menu_name": "首页",
    "menu_url": "home",
    "parent_id": null
  },
  {
    "id": "w1-1",
    "menu_name": "首页-工作台",
    "menu_url": "home.workbench",
    "parent_id": "w1"
  },
  {
    "id": "w1-2",
    "menu_name": "首页-工作日历",
    "menu_url": "home.calendar",
    "parent_id": "w1"
  },
  {
    "id": "w1-2/1",
    "menu_name": "首页-工作日历-面访",
    "menu_url": "home.calendar.interview",
    "parent_id": "w1-2"
  }
]
```
> 输出
```json
{
  "id": "w1",
  "menu_name": "首页",
  "menu_url": "home",
  "parent_id": null,
  "children": [{
      "id": "w1-1",
      "menu_name": "首页-工作台",
      "menu_url": "home.workbench",
      "parent_id": "w1",
      "children": []
    },
    {
      "id": "w1-2",
      "menu_name": "首页-工作日历",
      "menu_url": "home.calendar",
      "parent_id": "w1",
      "children": [{
        "id": "w1-2/1",
        "menu_name": "首页-工作日历-面访",
        "menu_url": "home.calendar.interview",
        "parent_id": "w1-2"
      }]
    }
  ]
}
```



## Q2 前K个高频关键词
## 描述
给定一个评论列表reviews，一个关键字列表 keywords 以及一个整数k。
找出在不同评论中出现次数最多的前k个关键词，这k个关键词按照出现次数的由多到少来排序。
字符串不区分大小写，如果关键字在不同评论中出现的次数相等，请按字母顺序从小到大排序。

- 如果K大于列表keywords的长度，则直接输出keywords
- keywords 的列表长度范围是: [1, 100]
- reviews 的列表长度范围是: [1: 1000]
- kewords[i] 由小写字母组成
- reviews[i] 由大小写字母以及标点符号: [ "[", "\", "!", "?", ",", ";" , ".", "]"

```javascript
示例 1
输入:
k = 2
keywords = ["anacell", "cetracular", "betacellular"]
reviews = [
  "Anacell provides the best services in the city",
  "betacellular has awesome services",
  "Best services provided by anacell, everyone should use anacell",
]
输出:
["anacell", "betacellular"]
解释: 
"anacell" 在2个不同的评论，"betacellular" 在1个评论中出现。
```

```javascript
示例 2:
输入:
k = 2
keywords = ["anacell", "betacellular", "cetracular", "deltacellular", "eurocell"]
reviews = [
  "I love anacell Best services; Best services provided by anacell",
  "betacellular has great services",
  "deltacellular provides much better services than betacellular",
  "cetracular is worse than anacell",
  "Betacellular is better than deltacellular.",
]
输出:
["betacellular", "anacell"]
解释: 
"betacellular" 在3个不同的评论中出现了，"anacell" 以及 "deltacellular" 在两个不同的评论中出现了，但是"anacell" 在字典序中最小。
```

## Q3 移动控制
## 描述
界面中存在id=jsContainer的节点A，系统会随机生成id为jsLayout的 m行 x n列 表格(m >= 1, n >= 1)，并随机选中一个td节点，请按照如下需求实现bind函数
1. bind 函数为document绑定keydown事件，当系统触发上(键值38)下(键值40)左(键值37)右(键值39)按键时，请找到当前选中的td节点，并根据当前指令切换高亮节点，具体效果参考以下图片
2. 在第一列往左移动则到达最后一列；在最后一列往右移动则到达第一列；在第一行往上移动则到达最后一行；在最后一行往下移动则到达第一行；
3. 请不要手动调用bind函数
4. 当前界面为系统在节点A中生成 9 * 9 表格并随机选中一个td节点后的效果
5. 请不要手动修改html和css，请不要修改js中的事件绑定方式
6. 不要使用第三方插件

> 请使用下面给出的html和css，不要修改，js文件基本结构如下

```html
<div id="jsContainer">
    <table class="game">
        <tbody>
            <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
            <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
            <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
            <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
            <tr><td></td><td></td><td></td><td></td><td class="current"></td><td></td><td></td><td></td><td></td></tr>
            <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
            <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
            <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
            <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        </tbody>
    </table>
</div>
```

```css
table.game {
    font-size: 14px;
    border-collapse: collapse;
    width: 100%;
    table-layout: fixed;
}
table.game td {
    border: 1px solid #e1e1e1;
    padding: 0;
    height: 30px;
    text-align: center;
}
table.game td.current{
    background: #1890ff;
}
```

```javascript
function bind() {

    document.onkeydown = event => {
        if (!event) return;
        var code = event.keyCode || '';
        if (!{'37': 1, '38': 1, '39': 1, '40': 1}[code]) return;
        event.preventDefault && event.preventDefault();
        //TODO: 请实现按键控制
    };
}
```

最终实现效果如下图所示


![Gif](https://uploadfiles.nowcoder.com/images/20200109/56_1578559651879_F79F98F69BF869A4E4CD476EE11795B1)